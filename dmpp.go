// Command cpp parses C preprocessor as defined by [1].
//
// BUGS <- many
//
// [1] <- http://www.externsoft.ch/download/cpp-iso-pp.html
package dmpp

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

func ParseDM(filename string) (string, error) {
	in := os.Stdin
	nm := "stdin"
	if len(filename) > 1 {
		f, err := os.Open(filename)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		in = f
		nm = filename
	}
	absPath, _ := filepath.Abs(filename)
	AddIncludePath(filepath.Dir(absPath))

	got, err := ParseReader(nm, in)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println("RESULT:")
	//fmt.Println(string(got.(string)))
	return got.(string), nil
}

type ppMacro struct {
	name  string
	value string
}
type ppMacrosMap map[string]ppMacro

var macrosMap ppMacrosMap = ppMacrosMap{}

func (mm ppMacrosMap) add(macro ppMacro) {
	mm[macro.name] = macro
}
func (mm ppMacrosMap) delete(name string) {
	delete(mm, name)
}

var includePathes []string

func AddIncludePath(s string) {
	includePathes = append(includePathes, s)
}
func searchInclude(fileName string) (string, error) {
	for _, v := range includePathes {
		p := path.Join(v, fileName)
		if _, err := os.Stat(p); err == nil {
			return p, nil
		}
	}
	return "", errors.New(fileName + " not found")
}
func parseInclude(fileName string) (string, error) {
	fileNameFull, err := searchInclude(fileName)
	//fmt.Println(fileNameFull)
	if err != nil {
		log.Fatal(err)
	}
	f, err := os.Open(fileNameFull)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}
	got, err := newParser(fileNameFull, b).parse(gProxy)
	return got.(string), err
}

var gProxy *grammar

func init() {
	gProxy = g
}

func toIfaceSlice(v interface{}) []interface{} {
	if v == nil {
		return nil
	}
	return v.([]interface{})
}

func IfaceSliceToString(v []interface{}) string {
	if v == nil {
		return ""
	}
	//fmt.Println(reflect.TypeOf(v));fmt.Println(v);

	b := make([]string, len(v))
	for i := range v {
		b[i] = v[i].(string)
	}

	s := ""
	for i := range b {
		s = s + b[i]
	}
	return s
}

func Uint8SliceToString(v interface{}) string {
	if v == nil {
		return ""
	}
	b := v.([]uint8)
	s := ""
	for i := range b {
		s = s + string(b[i])
	}
	return s
}

var ops = map[string]func(int, int) int{
	"+": func(l, r int) int {
		return l + r
	},
	"-": func(l, r int) int {
		return l - r
	},
	"*": func(l, r int) int {
		return l * r
	},
	"/": func(l, r int) int {
		return l / r
	},
	"%": func(l, r int) int {
		return l % r
	},
	"<<": func(l, r int) int {
		return l << uint(r)
	},
	">>": func(l, r int) int {
		return l >> uint(r)
	},
	"<=": func(l, r int) int {
		return BoolToInt(l <= r)
	},
	">=": func(l, r int) int {
		return BoolToInt(l >= r)
	},
	"<": func(l, r int) int {
		return BoolToInt(l < r)
	},
	">": func(l, r int) int {
		return BoolToInt(l > r)
	},
	"!=": func(l, r int) int {
		return BoolToInt(l != r)
	},
	"==": func(l, r int) int {
		return BoolToInt(l == r)
	},
	"&": func(l, r int) int {
		return l & r
	},
	"^": func(l, r int) int {
		return l ^ r
	},
	"&&": func(l, r int) int {
		return BoolToInt(IntToBool(l) && IntToBool(r))
	},
	"||": func(l, r int) int {
		return BoolToInt(IntToBool(l) || IntToBool(r))
	},
	"|": func(l, r int) int {
		return l | r
	},
}

func BoolToInt(b bool) int {
	if b {
		return 1
	}
	return 0
}
func IntToBool(i int) bool {
	return i != 0
}

func eval(first, rest interface{}) int {
	l := first.(int)
	restSl := toIfaceSlice(rest)
	for _, v := range restSl {
		restExpr := toIfaceSlice(v)
		r := restExpr[3].(int)
		op := restExpr[1].(string)
		l = ops[op](l, r)
	}
	return l
}

var g = &grammar{
	rules: []*rule{
		{
			name: "PreprocessingFile",
			pos:  position{line: 204, col: 1, offset: 4290},
			expr: &actionExpr{
				pos: position{line: 204, col: 22, offset: 4311},
				run: (*parser).callonPreprocessingFile1,
				expr: &seqExpr{
					pos: position{line: 204, col: 22, offset: 4311},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 204, col: 22, offset: 4311},
							label: "val",
							expr: &zeroOrOneExpr{
								pos: position{line: 204, col: 27, offset: 4316},
								expr: &ruleRefExpr{
									pos:  position{line: 204, col: 27, offset: 4316},
									name: "Group",
								},
							},
						},
						&zeroOrOneExpr{
							pos: position{line: 204, col: 35, offset: 4324},
							expr: &ruleRefExpr{
								pos:  position{line: 204, col: 35, offset: 4324},
								name: "EOF",
							},
						},
					},
				},
			},
		},
		{
			name: "Group",
			pos:  position{line: 205, col: 1, offset: 4359},
			expr: &actionExpr{
				pos: position{line: 205, col: 11, offset: 4369},
				run: (*parser).callonGroup1,
				expr: &labeledExpr{
					pos:   position{line: 205, col: 11, offset: 4369},
					label: "val",
					expr: &zeroOrMoreExpr{
						pos: position{line: 205, col: 16, offset: 4374},
						expr: &ruleRefExpr{
							pos:  position{line: 205, col: 16, offset: 4374},
							name: "GroupPart",
						},
					},
				},
			},
		},
		{
			name: "GroupPart",
			pos:  position{line: 206, col: 1, offset: 4444},
			expr: &actionExpr{
				pos: position{line: 206, col: 15, offset: 4458},
				run: (*parser).callonGroupPart1,
				expr: &labeledExpr{
					pos:   position{line: 206, col: 15, offset: 4458},
					label: "val",
					expr: &choiceExpr{
						pos: position{line: 206, col: 21, offset: 4464},
						alternatives: []interface{}{
							&ruleRefExpr{
								pos:  position{line: 206, col: 21, offset: 4464},
								name: "IfSection",
							},
							&ruleRefExpr{
								pos:  position{line: 206, col: 33, offset: 4476},
								name: "ControlLine",
							},
							&ruleRefExpr{
								pos:  position{line: 206, col: 47, offset: 4490},
								name: "OtherTokens",
							},
						},
					},
				},
			},
		},
		{
			name: "Comment",
			pos:  position{line: 208, col: 1, offset: 4537},
			expr: &actionExpr{
				pos: position{line: 208, col: 11, offset: 4547},
				run: (*parser).callonComment1,
				expr: &choiceExpr{
					pos: position{line: 208, col: 12, offset: 4548},
					alternatives: []interface{}{
						&seqExpr{
							pos: position{line: 208, col: 13, offset: 4549},
							exprs: []interface{}{
								&litMatcher{
									pos:        position{line: 208, col: 13, offset: 4549},
									val:        "//",
									ignoreCase: false,
								},
								&zeroOrMoreExpr{
									pos: position{line: 208, col: 18, offset: 4554},
									expr: &seqExpr{
										pos: position{line: 208, col: 19, offset: 4555},
										exprs: []interface{}{
											&notExpr{
												pos: position{line: 208, col: 19, offset: 4555},
												expr: &ruleRefExpr{
													pos:  position{line: 208, col: 20, offset: 4556},
													name: "NL",
												},
											},
											&anyMatcher{
												line: 208, col: 23, offset: 4559,
											},
										},
									},
								},
								&andExpr{
									pos: position{line: 208, col: 27, offset: 4563},
									expr: &ruleRefExpr{
										pos:  position{line: 208, col: 28, offset: 4564},
										name: "NL",
									},
								},
							},
						},
						&seqExpr{
							pos: position{line: 208, col: 37, offset: 4573},
							exprs: []interface{}{
								&litMatcher{
									pos:        position{line: 208, col: 37, offset: 4573},
									val:        "/*",
									ignoreCase: false,
								},
								&zeroOrMoreExpr{
									pos: position{line: 208, col: 42, offset: 4578},
									expr: &seqExpr{
										pos: position{line: 208, col: 43, offset: 4579},
										exprs: []interface{}{
											&notExpr{
												pos: position{line: 208, col: 43, offset: 4579},
												expr: &litMatcher{
													pos:        position{line: 208, col: 44, offset: 4580},
													val:        "*/",
													ignoreCase: false,
												},
											},
											&anyMatcher{
												line: 208, col: 49, offset: 4585,
											},
										},
									},
								},
								&litMatcher{
									pos:        position{line: 208, col: 53, offset: 4589},
									val:        "*/",
									ignoreCase: false,
								},
							},
						},
					},
				},
			},
		},
		{
			name: "OtherTokens",
			pos:  position{line: 210, col: 1, offset: 4624},
			expr: &actionExpr{
				pos: position{line: 210, col: 16, offset: 4639},
				run: (*parser).callonOtherTokens1,
				expr: &labeledExpr{
					pos:   position{line: 210, col: 16, offset: 4639},
					label: "val",
					expr: &seqExpr{
						pos: position{line: 210, col: 21, offset: 4644},
						exprs: []interface{}{
							&zeroOrOneExpr{
								pos: position{line: 210, col: 21, offset: 4644},
								expr: &ruleRefExpr{
									pos:  position{line: 210, col: 21, offset: 4644},
									name: "PPTokens",
								},
							},
							&ruleRefExpr{
								pos:  position{line: 210, col: 31, offset: 4654},
								name: "NL",
							},
						},
					},
				},
			},
		},
		{
			name: "PPTokens",
			pos:  position{line: 211, col: 1, offset: 4747},
			expr: &actionExpr{
				pos: position{line: 211, col: 13, offset: 4759},
				run: (*parser).callonPPTokens1,
				expr: &labeledExpr{
					pos:   position{line: 211, col: 13, offset: 4759},
					label: "val",
					expr: &zeroOrMoreExpr{
						pos: position{line: 211, col: 18, offset: 4764},
						expr: &ruleRefExpr{
							pos:  position{line: 211, col: 18, offset: 4764},
							name: "PreprocessingToken",
						},
					},
				},
			},
		},
		{
			name: "PreprocessingToken",
			pos:  position{line: 212, col: 1, offset: 4867},
			expr: &actionExpr{
				pos: position{line: 212, col: 23, offset: 4889},
				run: (*parser).callonPreprocessingToken1,
				expr: &labeledExpr{
					pos:   position{line: 212, col: 23, offset: 4889},
					label: "val",
					expr: &choiceExpr{
						pos: position{line: 212, col: 29, offset: 4895},
						alternatives: []interface{}{
							&ruleRefExpr{
								pos:  position{line: 212, col: 29, offset: 4895},
								name: "Comment",
							},
							&ruleRefExpr{
								pos:  position{line: 212, col: 39, offset: 4905},
								name: "Identifier",
							},
							&ruleRefExpr{
								pos:  position{line: 212, col: 52, offset: 4918},
								name: "CharacterLiteral",
							},
							&ruleRefExpr{
								pos:  position{line: 212, col: 71, offset: 4937},
								name: "StringLiteral",
							},
							&ruleRefExpr{
								pos:  position{line: 212, col: 87, offset: 4953},
								name: "ANY_NOT_YET_COVERED_NON_WSCHARACTER",
							},
						},
					},
				},
			},
		},
		{
			name: "ANY_NOT_YET_COVERED_NON_WSCHARACTER",
			pos:  position{line: 213, col: 1, offset: 5021},
			expr: &actionExpr{
				pos: position{line: 213, col: 40, offset: 5060},
				run: (*parser).callonANY_NOT_YET_COVERED_NON_WSCHARACTER1,
				expr: &charClassMatcher{
					pos:        position{line: 213, col: 40, offset: 5060},
					val:        "[^#\\x0d\\x0a]",
					chars:      []rune{'#', '\r', '\n'},
					ignoreCase: false,
					inverted:   true,
				},
			},
		},
		{
			name: "Identifier",
			pos:  position{line: 214, col: 1, offset: 5118},
			expr: &actionExpr{
				pos: position{line: 214, col: 15, offset: 5132},
				run: (*parser).callonIdentifier1,
				expr: &seqExpr{
					pos: position{line: 214, col: 15, offset: 5132},
					exprs: []interface{}{
						&ruleRefExpr{
							pos:  position{line: 214, col: 15, offset: 5132},
							name: "NonDigit",
						},
						&zeroOrMoreExpr{
							pos: position{line: 214, col: 24, offset: 5141},
							expr: &choiceExpr{
								pos: position{line: 214, col: 26, offset: 5143},
								alternatives: []interface{}{
									&ruleRefExpr{
										pos:  position{line: 214, col: 26, offset: 5143},
										name: "NonDigit",
									},
									&ruleRefExpr{
										pos:  position{line: 214, col: 37, offset: 5154},
										name: "Digit",
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "NonDigit",
			pos:  position{line: 221, col: 1, offset: 5310},
			expr: &charClassMatcher{
				pos:        position{line: 221, col: 13, offset: 5322},
				val:        "[a-z_]i",
				chars:      []rune{'_'},
				ranges:     []rune{'a', 'z'},
				ignoreCase: true,
				inverted:   false,
			},
		},
		{
			name: "Digit",
			pos:  position{line: 222, col: 1, offset: 5331},
			expr: &charClassMatcher{
				pos:        position{line: 222, col: 10, offset: 5340},
				val:        "[0-9]",
				ranges:     []rune{'0', '9'},
				ignoreCase: false,
				inverted:   false,
			},
		},
		{
			name: "Sign",
			pos:  position{line: 223, col: 1, offset: 5347},
			expr: &charClassMatcher{
				pos:        position{line: 223, col: 9, offset: 5355},
				val:        "[+-]",
				chars:      []rune{'+', '-'},
				ignoreCase: false,
				inverted:   false,
			},
		},
		{
			name: "CharacterLiteral",
			pos:  position{line: 225, col: 1, offset: 5363},
			expr: &actionExpr{
				pos: position{line: 225, col: 21, offset: 5383},
				run: (*parser).callonCharacterLiteral1,
				expr: &seqExpr{
					pos: position{line: 225, col: 21, offset: 5383},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 225, col: 21, offset: 5383},
							val:        "'",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 225, col: 25, offset: 5387},
							name: "CCharSequence",
						},
						&litMatcher{
							pos:        position{line: 225, col: 39, offset: 5401},
							val:        "'",
							ignoreCase: false,
						},
					},
				},
			},
		},
		{
			name: "CCharSequence",
			pos:  position{line: 226, col: 1, offset: 5460},
			expr: &oneOrMoreExpr{
				pos: position{line: 226, col: 21, offset: 5480},
				expr: &ruleRefExpr{
					pos:  position{line: 226, col: 21, offset: 5480},
					name: "CChar",
				},
			},
		},
		{
			name: "CChar",
			pos:  position{line: 227, col: 1, offset: 5488},
			expr: &choiceExpr{
				pos: position{line: 227, col: 21, offset: 5508},
				alternatives: []interface{}{
					&ruleRefExpr{
						pos:  position{line: 227, col: 21, offset: 5508},
						name: "EscapeSequence",
					},
					&ruleRefExpr{
						pos:  position{line: 227, col: 38, offset: 5525},
						name: "AnyExceptNlSqBs",
					},
				},
			},
		},
		{
			name: "AnyExceptNlSqBs",
			pos:  position{line: 228, col: 1, offset: 5543},
			expr: &charClassMatcher{
				pos:        position{line: 228, col: 20, offset: 5562},
				val:        "[^\\x0d\\x0a'\\\\]",
				chars:      []rune{'\r', '\n', '\'', '\\'},
				ignoreCase: false,
				inverted:   true,
			},
		},
		{
			name: "StringLiteral",
			pos:  position{line: 229, col: 1, offset: 5578},
			expr: &actionExpr{
				pos: position{line: 229, col: 20, offset: 5597},
				run: (*parser).callonStringLiteral1,
				expr: &seqExpr{
					pos: position{line: 229, col: 20, offset: 5597},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 229, col: 20, offset: 5597},
							val:        "\"",
							ignoreCase: false,
						},
						&labeledExpr{
							pos:   position{line: 229, col: 25, offset: 5602},
							label: "val",
							expr: &zeroOrOneExpr{
								pos: position{line: 229, col: 29, offset: 5606},
								expr: &ruleRefExpr{
									pos:  position{line: 229, col: 29, offset: 5606},
									name: "SCharSequence",
								},
							},
						},
						&litMatcher{
							pos:        position{line: 229, col: 44, offset: 5621},
							val:        "\"",
							ignoreCase: false,
						},
					},
				},
			},
		},
		{
			name: "SCharSequence",
			pos:  position{line: 230, col: 1, offset: 5681},
			expr: &actionExpr{
				pos: position{line: 230, col: 20, offset: 5700},
				run: (*parser).callonSCharSequence1,
				expr: &labeledExpr{
					pos:   position{line: 230, col: 20, offset: 5700},
					label: "val",
					expr: &oneOrMoreExpr{
						pos: position{line: 230, col: 24, offset: 5704},
						expr: &ruleRefExpr{
							pos:  position{line: 230, col: 24, offset: 5704},
							name: "SChar",
						},
					},
				},
			},
		},
		{
			name: "SChar",
			pos:  position{line: 231, col: 1, offset: 5801},
			expr: &actionExpr{
				pos: position{line: 231, col: 20, offset: 5820},
				run: (*parser).callonSChar1,
				expr: &labeledExpr{
					pos:   position{line: 231, col: 20, offset: 5820},
					label: "val",
					expr: &choiceExpr{
						pos: position{line: 231, col: 25, offset: 5825},
						alternatives: []interface{}{
							&ruleRefExpr{
								pos:  position{line: 231, col: 25, offset: 5825},
								name: "EmbeddedLiteral",
							},
							&ruleRefExpr{
								pos:  position{line: 231, col: 43, offset: 5843},
								name: "EscapeSequence",
							},
							&ruleRefExpr{
								pos:  position{line: 231, col: 60, offset: 5860},
								name: "AnyExceptNlDqBs",
							},
						},
					},
				},
			},
		},
		{
			name: "AnyExceptNlDqBs",
			pos:  position{line: 232, col: 1, offset: 5914},
			expr: &actionExpr{
				pos: position{line: 232, col: 20, offset: 5933},
				run: (*parser).callonAnyExceptNlDqBs1,
				expr: &charClassMatcher{
					pos:        position{line: 232, col: 20, offset: 5933},
					val:        "[^\\x0d\\x0a\"\\\\]",
					chars:      []rune{'\r', '\n', '"', '\\'},
					ignoreCase: false,
					inverted:   true,
				},
			},
		},
		{
			name: "EscapeSequence",
			pos:  position{line: 233, col: 1, offset: 6026},
			expr: &actionExpr{
				pos: position{line: 233, col: 28, offset: 6053},
				run: (*parser).callonEscapeSequence1,
				expr: &choiceExpr{
					pos: position{line: 233, col: 29, offset: 6054},
					alternatives: []interface{}{
						&ruleRefExpr{
							pos:  position{line: 233, col: 29, offset: 6054},
							name: "SimpleEscapeSequence",
						},
						&ruleRefExpr{
							pos:  position{line: 233, col: 52, offset: 6077},
							name: "HexadecimalEscapeSequence",
						},
					},
				},
			},
		},
		{
			name: "SimpleEscapeSequence",
			pos:  position{line: 234, col: 1, offset: 6138},
			expr: &seqExpr{
				pos: position{line: 234, col: 28, offset: 6165},
				exprs: []interface{}{
					&litMatcher{
						pos:        position{line: 234, col: 28, offset: 6165},
						val:        "\\",
						ignoreCase: false,
					},
					&charClassMatcher{
						pos:        position{line: 234, col: 33, offset: 6170},
						val:        "[\"\\\\/bfnrt]",
						chars:      []rune{'"', '\\', '/', 'b', 'f', 'n', 'r', 't'},
						ignoreCase: false,
						inverted:   false,
					},
				},
			},
		},
		{
			name: "HexadecimalEscapeSequence",
			pos:  position{line: 235, col: 1, offset: 6183},
			expr: &seqExpr{
				pos: position{line: 235, col: 30, offset: 6212},
				exprs: []interface{}{
					&litMatcher{
						pos:        position{line: 235, col: 30, offset: 6212},
						val:        "\\",
						ignoreCase: false,
					},
					&litMatcher{
						pos:        position{line: 235, col: 35, offset: 6217},
						val:        "x",
						ignoreCase: false,
					},
					&oneOrMoreExpr{
						pos: position{line: 235, col: 39, offset: 6221},
						expr: &ruleRefExpr{
							pos:  position{line: 235, col: 39, offset: 6221},
							name: "HexadecimalDigit",
						},
					},
				},
			},
		},
		{
			name: "HexadecimalDigit",
			pos:  position{line: 236, col: 1, offset: 6240},
			expr: &charClassMatcher{
				pos:        position{line: 236, col: 28, offset: 6267},
				val:        "[0-9a-f]i",
				ranges:     []rune{'0', '9', 'a', 'f'},
				ignoreCase: true,
				inverted:   false,
			},
		},
		{
			name: "EmbeddedLiteral",
			pos:  position{line: 237, col: 1, offset: 6278},
			expr: &actionExpr{
				pos: position{line: 237, col: 23, offset: 6300},
				run: (*parser).callonEmbeddedLiteral1,
				expr: &seqExpr{
					pos: position{line: 237, col: 23, offset: 6300},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 237, col: 23, offset: 6300},
							val:        "[",
							ignoreCase: false,
						},
						&labeledExpr{
							pos:   position{line: 237, col: 27, offset: 6304},
							label: "val",
							expr: &ruleRefExpr{
								pos:  position{line: 237, col: 31, offset: 6308},
								name: "EmbeddedExpr",
							},
						},
						&litMatcher{
							pos:        position{line: 237, col: 44, offset: 6321},
							val:        "]",
							ignoreCase: false,
						},
					},
				},
			},
		},
		{
			name: "EmbeddedExpr",
			pos:  position{line: 243, col: 1, offset: 6433},
			expr: &actionExpr{
				pos: position{line: 243, col: 17, offset: 6449},
				run: (*parser).callonEmbeddedExpr1,
				expr: &labeledExpr{
					pos:   position{line: 243, col: 17, offset: 6449},
					label: "val",
					expr: &zeroOrMoreExpr{
						pos: position{line: 243, col: 21, offset: 6453},
						expr: &seqExpr{
							pos: position{line: 243, col: 22, offset: 6454},
							exprs: []interface{}{
								&notExpr{
									pos: position{line: 243, col: 22, offset: 6454},
									expr: &litMatcher{
										pos:        position{line: 243, col: 23, offset: 6455},
										val:        "]",
										ignoreCase: false,
									},
								},
								&ruleRefExpr{
									pos:  position{line: 243, col: 27, offset: 6459},
									name: "PreprocessingToken",
								},
							},
						},
					},
				},
			},
		},
		{
			name: "IfSection",
			pos:  position{line: 254, col: 1, offset: 6664},
			expr: &actionExpr{
				pos: position{line: 254, col: 20, offset: 6683},
				run: (*parser).callonIfSection1,
				expr: &seqExpr{
					pos: position{line: 254, col: 20, offset: 6683},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 254, col: 20, offset: 6683},
							label: "ifval",
							expr: &ruleRefExpr{
								pos:  position{line: 254, col: 27, offset: 6690},
								name: "IfGroup",
							},
						},
						&labeledExpr{
							pos:   position{line: 254, col: 37, offset: 6700},
							label: "elifval",
							expr: &zeroOrMoreExpr{
								pos: position{line: 254, col: 46, offset: 6709},
								expr: &ruleRefExpr{
									pos:  position{line: 254, col: 46, offset: 6709},
									name: "ElIfGroup",
								},
							},
						},
						&labeledExpr{
							pos:   position{line: 254, col: 58, offset: 6721},
							label: "elseval",
							expr: &zeroOrOneExpr{
								pos: position{line: 254, col: 67, offset: 6730},
								expr: &ruleRefExpr{
									pos:  position{line: 254, col: 67, offset: 6730},
									name: "ElseGroup",
								},
							},
						},
						&ruleRefExpr{
							pos:  position{line: 254, col: 79, offset: 6742},
							name: "EndifLine",
						},
					},
				},
			},
		},
		{
			name: "IfGroup",
			pos:  position{line: 270, col: 1, offset: 7127},
			expr: &actionExpr{
				pos: position{line: 270, col: 20, offset: 7146},
				run: (*parser).callonIfGroup1,
				expr: &seqExpr{
					pos: position{line: 270, col: 20, offset: 7146},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 270, col: 20, offset: 7146},
							val:        "#",
							ignoreCase: false,
						},
						&labeledExpr{
							pos:   position{line: 270, col: 24, offset: 7150},
							label: "ex",
							expr: &choiceExpr{
								pos: position{line: 270, col: 28, offset: 7154},
								alternatives: []interface{}{
									&seqExpr{
										pos: position{line: 270, col: 29, offset: 7155},
										exprs: []interface{}{
											&litMatcher{
												pos:        position{line: 270, col: 29, offset: 7155},
												val:        "ifdef",
												ignoreCase: false,
											},
											&ruleRefExpr{
												pos:  position{line: 270, col: 37, offset: 7163},
												name: "_",
											},
											&ruleRefExpr{
												pos:  position{line: 270, col: 39, offset: 7165},
												name: "IfDefMacro",
											},
										},
									},
									&seqExpr{
										pos: position{line: 270, col: 55, offset: 7181},
										exprs: []interface{}{
											&litMatcher{
												pos:        position{line: 270, col: 55, offset: 7181},
												val:        "if",
												ignoreCase: false,
											},
											&ruleRefExpr{
												pos:  position{line: 270, col: 60, offset: 7186},
												name: "_",
											},
											&ruleRefExpr{
												pos:  position{line: 270, col: 62, offset: 7188},
												name: "PPExpression",
											},
										},
									},
								},
							},
						},
						&ruleRefExpr{
							pos:  position{line: 270, col: 77, offset: 7203},
							name: "_",
						},
						&ruleRefExpr{
							pos:  position{line: 270, col: 79, offset: 7205},
							name: "NL",
						},
						&labeledExpr{
							pos:   position{line: 270, col: 82, offset: 7208},
							label: "group",
							expr: &zeroOrOneExpr{
								pos: position{line: 270, col: 89, offset: 7215},
								expr: &ruleRefExpr{
									pos:  position{line: 270, col: 89, offset: 7215},
									name: "Group",
								},
							},
						},
					},
				},
			},
		},
		{
			name: "IfDefMacro",
			pos:  position{line: 288, col: 1, offset: 7689},
			expr: &actionExpr{
				pos: position{line: 288, col: 15, offset: 7703},
				run: (*parser).callonIfDefMacro1,
				expr: &ruleRefExpr{
					pos:  position{line: 288, col: 15, offset: 7703},
					name: "Identifier",
				},
			},
		},
		{
			name: "ElIfGroup",
			pos:  position{line: 289, col: 1, offset: 7793},
			expr: &actionExpr{
				pos: position{line: 289, col: 20, offset: 7812},
				run: (*parser).callonElIfGroup1,
				expr: &seqExpr{
					pos: position{line: 289, col: 20, offset: 7812},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 289, col: 20, offset: 7812},
							val:        "#",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 289, col: 24, offset: 7816},
							val:        "elif",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 289, col: 31, offset: 7823},
							name: "_",
						},
						&labeledExpr{
							pos:   position{line: 289, col: 33, offset: 7825},
							label: "val",
							expr: &ruleRefExpr{
								pos:  position{line: 289, col: 37, offset: 7829},
								name: "PPExpression",
							},
						},
						&ruleRefExpr{
							pos:  position{line: 289, col: 50, offset: 7842},
							name: "_",
						},
						&ruleRefExpr{
							pos:  position{line: 289, col: 52, offset: 7844},
							name: "NL",
						},
						&labeledExpr{
							pos:   position{line: 289, col: 55, offset: 7847},
							label: "group",
							expr: &zeroOrOneExpr{
								pos: position{line: 289, col: 61, offset: 7853},
								expr: &ruleRefExpr{
									pos:  position{line: 289, col: 61, offset: 7853},
									name: "Group",
								},
							},
						},
					},
				},
			},
		},
		{
			name: "ElseGroup",
			pos:  position{line: 295, col: 1, offset: 7957},
			expr: &actionExpr{
				pos: position{line: 295, col: 20, offset: 7976},
				run: (*parser).callonElseGroup1,
				expr: &seqExpr{
					pos: position{line: 295, col: 20, offset: 7976},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 295, col: 20, offset: 7976},
							val:        "#",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 295, col: 24, offset: 7980},
							val:        "else",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 295, col: 31, offset: 7987},
							name: "_",
						},
						&ruleRefExpr{
							pos:  position{line: 295, col: 33, offset: 7989},
							name: "NL",
						},
						&labeledExpr{
							pos:   position{line: 295, col: 36, offset: 7992},
							label: "group",
							expr: &zeroOrOneExpr{
								pos: position{line: 295, col: 42, offset: 7998},
								expr: &ruleRefExpr{
									pos:  position{line: 295, col: 42, offset: 7998},
									name: "Group",
								},
							},
						},
					},
				},
			},
		},
		{
			name: "EndifLine",
			pos:  position{line: 296, col: 1, offset: 8061},
			expr: &actionExpr{
				pos: position{line: 296, col: 20, offset: 8080},
				run: (*parser).callonEndifLine1,
				expr: &seqExpr{
					pos: position{line: 296, col: 20, offset: 8080},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 296, col: 20, offset: 8080},
							val:        "#",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 296, col: 24, offset: 8084},
							val:        "endif",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 296, col: 32, offset: 8092},
							name: "_",
						},
						&ruleRefExpr{
							pos:  position{line: 296, col: 34, offset: 8094},
							name: "NL",
						},
					},
				},
			},
		},
		{
			name: "PPExpression",
			pos:  position{line: 298, col: 1, offset: 8155},
			expr: &actionExpr{
				pos: position{line: 298, col: 28, offset: 8182},
				run: (*parser).callonPPExpression1,
				expr: &labeledExpr{
					pos:   position{line: 298, col: 28, offset: 8182},
					label: "ex",
					expr: &ruleRefExpr{
						pos:  position{line: 298, col: 31, offset: 8185},
						name: "ConditionalExpression",
					},
				},
			},
		},
		{
			name: "ConditionalExpression",
			pos:  position{line: 299, col: 1, offset: 8248},
			expr: &actionExpr{
				pos: position{line: 299, col: 28, offset: 8275},
				run: (*parser).callonConditionalExpression1,
				expr: &seqExpr{
					pos: position{line: 299, col: 28, offset: 8275},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 299, col: 28, offset: 8275},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 299, col: 31, offset: 8278},
								name: "LogicalOrExpression",
							},
						},
						&zeroOrOneExpr{
							pos: position{line: 299, col: 51, offset: 8298},
							expr: &seqExpr{
								pos: position{line: 299, col: 53, offset: 8300},
								exprs: []interface{}{
									&ruleRefExpr{
										pos:  position{line: 299, col: 53, offset: 8300},
										name: "_",
									},
									&litMatcher{
										pos:        position{line: 299, col: 55, offset: 8302},
										val:        "?",
										ignoreCase: false,
									},
									&ruleRefExpr{
										pos:  position{line: 299, col: 59, offset: 8306},
										name: "_",
									},
									&ruleRefExpr{
										pos:  position{line: 299, col: 61, offset: 8308},
										name: "PPExpression",
									},
									&ruleRefExpr{
										pos:  position{line: 299, col: 74, offset: 8321},
										name: "_",
									},
									&litMatcher{
										pos:        position{line: 299, col: 76, offset: 8323},
										val:        ":",
										ignoreCase: false,
									},
									&ruleRefExpr{
										pos:  position{line: 299, col: 80, offset: 8327},
										name: "_",
									},
									&ruleRefExpr{
										pos:  position{line: 299, col: 82, offset: 8329},
										name: "PPExpression",
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "LogicalOrExpression",
			pos:  position{line: 300, col: 1, offset: 8375},
			expr: &actionExpr{
				pos: position{line: 300, col: 28, offset: 8402},
				run: (*parser).callonLogicalOrExpression1,
				expr: &seqExpr{
					pos: position{line: 300, col: 28, offset: 8402},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 300, col: 28, offset: 8402},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 300, col: 31, offset: 8405},
								name: "LogicalAndExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 300, col: 52, offset: 8426},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 300, col: 57, offset: 8431},
								expr: &seqExpr{
									pos: position{line: 300, col: 59, offset: 8433},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 300, col: 59, offset: 8433},
											name: "_",
										},
										&litMatcher{
											pos:        position{line: 300, col: 61, offset: 8435},
											val:        "||",
											ignoreCase: false,
										},
										&ruleRefExpr{
											pos:  position{line: 300, col: 66, offset: 8440},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 300, col: 68, offset: 8442},
											name: "LogicalAndExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "LogicalAndExpression",
			pos:  position{line: 301, col: 1, offset: 8514},
			expr: &actionExpr{
				pos: position{line: 301, col: 28, offset: 8541},
				run: (*parser).callonLogicalAndExpression1,
				expr: &seqExpr{
					pos: position{line: 301, col: 28, offset: 8541},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 301, col: 28, offset: 8541},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 301, col: 31, offset: 8544},
								name: "InclusiveOrExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 301, col: 53, offset: 8566},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 301, col: 58, offset: 8571},
								expr: &seqExpr{
									pos: position{line: 301, col: 60, offset: 8573},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 301, col: 60, offset: 8573},
											name: "_",
										},
										&litMatcher{
											pos:        position{line: 301, col: 62, offset: 8575},
											val:        "&&",
											ignoreCase: false,
										},
										&ruleRefExpr{
											pos:  position{line: 301, col: 67, offset: 8580},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 301, col: 69, offset: 8582},
											name: "InclusiveOrExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "InclusiveOrExpression",
			pos:  position{line: 302, col: 1, offset: 8653},
			expr: &actionExpr{
				pos: position{line: 302, col: 28, offset: 8680},
				run: (*parser).callonInclusiveOrExpression1,
				expr: &seqExpr{
					pos: position{line: 302, col: 28, offset: 8680},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 302, col: 28, offset: 8680},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 302, col: 31, offset: 8683},
								name: "ExclusiveOrExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 302, col: 53, offset: 8705},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 302, col: 58, offset: 8710},
								expr: &seqExpr{
									pos: position{line: 302, col: 60, offset: 8712},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 302, col: 60, offset: 8712},
											name: "_",
										},
										&litMatcher{
											pos:        position{line: 302, col: 62, offset: 8714},
											val:        "|",
											ignoreCase: false,
										},
										&ruleRefExpr{
											pos:  position{line: 302, col: 66, offset: 8718},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 302, col: 68, offset: 8720},
											name: "ExclusiveOrExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "ExclusiveOrExpression",
			pos:  position{line: 303, col: 1, offset: 8792},
			expr: &actionExpr{
				pos: position{line: 303, col: 28, offset: 8819},
				run: (*parser).callonExclusiveOrExpression1,
				expr: &seqExpr{
					pos: position{line: 303, col: 28, offset: 8819},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 303, col: 28, offset: 8819},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 303, col: 31, offset: 8822},
								name: "AndExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 303, col: 45, offset: 8836},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 303, col: 50, offset: 8841},
								expr: &seqExpr{
									pos: position{line: 303, col: 52, offset: 8843},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 303, col: 52, offset: 8843},
											name: "_",
										},
										&litMatcher{
											pos:        position{line: 303, col: 54, offset: 8845},
											val:        "^",
											ignoreCase: false,
										},
										&ruleRefExpr{
											pos:  position{line: 303, col: 58, offset: 8849},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 303, col: 60, offset: 8851},
											name: "AndExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "AndExpression",
			pos:  position{line: 304, col: 1, offset: 8931},
			expr: &actionExpr{
				pos: position{line: 304, col: 28, offset: 8958},
				run: (*parser).callonAndExpression1,
				expr: &seqExpr{
					pos: position{line: 304, col: 28, offset: 8958},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 304, col: 28, offset: 8958},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 304, col: 31, offset: 8961},
								name: "EqualityExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 304, col: 50, offset: 8980},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 304, col: 55, offset: 8985},
								expr: &seqExpr{
									pos: position{line: 304, col: 57, offset: 8987},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 304, col: 57, offset: 8987},
											name: "_",
										},
										&litMatcher{
											pos:        position{line: 304, col: 59, offset: 8989},
											val:        "&",
											ignoreCase: false,
										},
										&ruleRefExpr{
											pos:  position{line: 304, col: 63, offset: 8993},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 304, col: 65, offset: 8995},
											name: "EqualityExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "EqualityExpression",
			pos:  position{line: 305, col: 1, offset: 9070},
			expr: &actionExpr{
				pos: position{line: 305, col: 28, offset: 9097},
				run: (*parser).callonEqualityExpression1,
				expr: &seqExpr{
					pos: position{line: 305, col: 28, offset: 9097},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 305, col: 28, offset: 9097},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 305, col: 31, offset: 9100},
								name: "RelationalExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 305, col: 52, offset: 9121},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 305, col: 57, offset: 9126},
								expr: &seqExpr{
									pos: position{line: 305, col: 59, offset: 9128},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 305, col: 59, offset: 9128},
											name: "_",
										},
										&choiceExpr{
											pos: position{line: 305, col: 63, offset: 9132},
											alternatives: []interface{}{
												&litMatcher{
													pos:        position{line: 305, col: 63, offset: 9132},
													val:        "==",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 305, col: 70, offset: 9139},
													val:        "!=",
													ignoreCase: false,
												},
											},
										},
										&ruleRefExpr{
											pos:  position{line: 305, col: 77, offset: 9146},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 305, col: 79, offset: 9148},
											name: "RelationalExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "RelationalExpression",
			pos:  position{line: 306, col: 1, offset: 9209},
			expr: &actionExpr{
				pos: position{line: 306, col: 28, offset: 9236},
				run: (*parser).callonRelationalExpression1,
				expr: &seqExpr{
					pos: position{line: 306, col: 28, offset: 9236},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 306, col: 28, offset: 9236},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 306, col: 31, offset: 9239},
								name: "ShiftExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 306, col: 47, offset: 9255},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 306, col: 52, offset: 9260},
								expr: &seqExpr{
									pos: position{line: 306, col: 54, offset: 9262},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 306, col: 54, offset: 9262},
											name: "_",
										},
										&choiceExpr{
											pos: position{line: 306, col: 58, offset: 9266},
											alternatives: []interface{}{
												&litMatcher{
													pos:        position{line: 306, col: 58, offset: 9266},
													val:        "<=",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 306, col: 65, offset: 9273},
													val:        ">=",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 306, col: 72, offset: 9280},
													val:        "<",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 306, col: 78, offset: 9286},
													val:        ">",
													ignoreCase: false,
												},
											},
										},
										&ruleRefExpr{
											pos:  position{line: 306, col: 84, offset: 9292},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 306, col: 86, offset: 9294},
											name: "ShiftExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "ShiftExpression",
			pos:  position{line: 307, col: 1, offset: 9352},
			expr: &actionExpr{
				pos: position{line: 307, col: 28, offset: 9379},
				run: (*parser).callonShiftExpression1,
				expr: &seqExpr{
					pos: position{line: 307, col: 28, offset: 9379},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 307, col: 28, offset: 9379},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 307, col: 31, offset: 9382},
								name: "AdditiveExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 307, col: 50, offset: 9401},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 307, col: 55, offset: 9406},
								expr: &seqExpr{
									pos: position{line: 307, col: 57, offset: 9408},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 307, col: 57, offset: 9408},
											name: "_",
										},
										&choiceExpr{
											pos: position{line: 307, col: 61, offset: 9412},
											alternatives: []interface{}{
												&litMatcher{
													pos:        position{line: 307, col: 61, offset: 9412},
													val:        "<<",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 307, col: 68, offset: 9419},
													val:        ">>",
													ignoreCase: false,
												},
											},
										},
										&ruleRefExpr{
											pos:  position{line: 307, col: 75, offset: 9426},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 307, col: 77, offset: 9428},
											name: "AdditiveExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "AdditiveExpression",
			pos:  position{line: 308, col: 1, offset: 9491},
			expr: &actionExpr{
				pos: position{line: 308, col: 28, offset: 9518},
				run: (*parser).callonAdditiveExpression1,
				expr: &seqExpr{
					pos: position{line: 308, col: 28, offset: 9518},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 308, col: 28, offset: 9518},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 308, col: 31, offset: 9521},
								name: "MultiplicativeExpression",
							},
						},
						&labeledExpr{
							pos:   position{line: 308, col: 56, offset: 9546},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 308, col: 61, offset: 9551},
								expr: &seqExpr{
									pos: position{line: 308, col: 63, offset: 9553},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 308, col: 63, offset: 9553},
											name: "_",
										},
										&choiceExpr{
											pos: position{line: 308, col: 67, offset: 9557},
											alternatives: []interface{}{
												&litMatcher{
													pos:        position{line: 308, col: 67, offset: 9557},
													val:        "+",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 308, col: 73, offset: 9563},
													val:        "-",
													ignoreCase: false,
												},
											},
										},
										&ruleRefExpr{
											pos:  position{line: 308, col: 79, offset: 9569},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 308, col: 81, offset: 9571},
											name: "MultiplicativeExpression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "MultiplicativeExpression",
			pos:  position{line: 309, col: 1, offset: 9630},
			expr: &actionExpr{
				pos: position{line: 309, col: 29, offset: 9658},
				run: (*parser).callonMultiplicativeExpression1,
				expr: &seqExpr{
					pos: position{line: 309, col: 29, offset: 9658},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 309, col: 29, offset: 9658},
							label: "ex",
							expr: &ruleRefExpr{
								pos:  position{line: 309, col: 32, offset: 9661},
								name: "Unary_Expression",
							},
						},
						&labeledExpr{
							pos:   position{line: 309, col: 49, offset: 9678},
							label: "rest",
							expr: &zeroOrMoreExpr{
								pos: position{line: 309, col: 54, offset: 9683},
								expr: &seqExpr{
									pos: position{line: 309, col: 56, offset: 9685},
									exprs: []interface{}{
										&ruleRefExpr{
											pos:  position{line: 309, col: 56, offset: 9685},
											name: "_",
										},
										&choiceExpr{
											pos: position{line: 309, col: 60, offset: 9689},
											alternatives: []interface{}{
												&litMatcher{
													pos:        position{line: 309, col: 60, offset: 9689},
													val:        "*",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 309, col: 66, offset: 9695},
													val:        "/",
													ignoreCase: false,
												},
												&litMatcher{
													pos:        position{line: 309, col: 72, offset: 9701},
													val:        "%",
													ignoreCase: false,
												},
											},
										},
										&ruleRefExpr{
											pos:  position{line: 309, col: 78, offset: 9707},
											name: "_",
										},
										&ruleRefExpr{
											pos:  position{line: 309, col: 80, offset: 9709},
											name: "Unary_Expression",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "Unary_Expression",
			pos:  position{line: 310, col: 1, offset: 9769},
			expr: &actionExpr{
				pos: position{line: 310, col: 28, offset: 9796},
				run: (*parser).callonUnary_Expression1,
				expr: &seqExpr{
					pos: position{line: 310, col: 28, offset: 9796},
					exprs: []interface{}{
						&labeledExpr{
							pos:   position{line: 310, col: 28, offset: 9796},
							label: "unop",
							expr: &zeroOrOneExpr{
								pos: position{line: 310, col: 34, offset: 9802},
								expr: &ruleRefExpr{
									pos:  position{line: 310, col: 34, offset: 9802},
									name: "UnaryOperator",
								},
							},
						},
						&labeledExpr{
							pos:   position{line: 310, col: 50, offset: 9818},
							label: "val",
							expr: &ruleRefExpr{
								pos:  position{line: 310, col: 54, offset: 9822},
								name: "PrimaryExpression",
							},
						},
					},
				},
			},
		},
		{
			name: "UnaryOperator",
			pos:  position{line: 328, col: 1, offset: 10127},
			expr: &actionExpr{
				pos: position{line: 328, col: 28, offset: 10154},
				run: (*parser).callonUnaryOperator1,
				expr: &choiceExpr{
					pos: position{line: 328, col: 29, offset: 10155},
					alternatives: []interface{}{
						&litMatcher{
							pos:        position{line: 328, col: 29, offset: 10155},
							val:        "+",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 328, col: 35, offset: 10161},
							val:        "-",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 328, col: 41, offset: 10167},
							val:        "!",
							ignoreCase: false,
						},
						&litMatcher{
							pos:        position{line: 328, col: 47, offset: 10173},
							val:        "~",
							ignoreCase: false,
						},
					},
				},
			},
		},
		{
			name: "PrimaryExpression",
			pos:  position{line: 329, col: 1, offset: 10232},
			expr: &choiceExpr{
				pos: position{line: 329, col: 28, offset: 10259},
				alternatives: []interface{}{
					&actionExpr{
						pos: position{line: 329, col: 28, offset: 10259},
						run: (*parser).callonPrimaryExpression2,
						expr: &seqExpr{
							pos: position{line: 329, col: 28, offset: 10259},
							exprs: []interface{}{
								&litMatcher{
									pos:        position{line: 329, col: 28, offset: 10259},
									val:        "defined",
									ignoreCase: false,
								},
								&litMatcher{
									pos:        position{line: 329, col: 38, offset: 10269},
									val:        "(",
									ignoreCase: false,
								},
								&ruleRefExpr{
									pos:  position{line: 329, col: 42, offset: 10273},
									name: "_",
								},
								&labeledExpr{
									pos:   position{line: 329, col: 44, offset: 10275},
									label: "val",
									expr: &ruleRefExpr{
										pos:  position{line: 329, col: 48, offset: 10279},
										name: "Identifier",
									},
								},
								&ruleRefExpr{
									pos:  position{line: 329, col: 59, offset: 10290},
									name: "_",
								},
								&litMatcher{
									pos:        position{line: 329, col: 61, offset: 10292},
									val:        ")",
									ignoreCase: false,
								},
							},
						},
					},
					&actionExpr{
						pos: position{line: 336, col: 28, offset: 10427},
						run: (*parser).callonPrimaryExpression11,
						expr: &ruleRefExpr{
							pos:  position{line: 336, col: 28, offset: 10427},
							name: "IntegerLiteral",
						},
					},
					&actionExpr{
						pos: position{line: 338, col: 28, offset: 10604},
						run: (*parser).callonPrimaryExpression13,
						expr: &seqExpr{
							pos: position{line: 338, col: 28, offset: 10604},
							exprs: []interface{}{
								&litMatcher{
									pos:        position{line: 338, col: 28, offset: 10604},
									val:        "(",
									ignoreCase: false,
								},
								&ruleRefExpr{
									pos:  position{line: 338, col: 32, offset: 10608},
									name: "_",
								},
								&labeledExpr{
									pos:   position{line: 338, col: 35, offset: 10611},
									label: "val",
									expr: &ruleRefExpr{
										pos:  position{line: 338, col: 39, offset: 10615},
										name: "PPExpression",
									},
								},
								&ruleRefExpr{
									pos:  position{line: 338, col: 52, offset: 10628},
									name: "_",
								},
								&litMatcher{
									pos:        position{line: 338, col: 54, offset: 10630},
									val:        ")",
									ignoreCase: false,
								},
							},
						},
					},
					&actionExpr{
						pos: position{line: 341, col: 28, offset: 10694},
						run: (*parser).callonPrimaryExpression21,
						expr: &labeledExpr{
							pos:   position{line: 341, col: 28, offset: 10694},
							label: "val",
							expr: &ruleRefExpr{
								pos:  position{line: 341, col: 32, offset: 10698},
								name: "Identifier",
							},
						},
					},
				},
			},
		},
		{
			name: "IntegerLiteral",
			pos:  position{line: 349, col: 1, offset: 10914},
			expr: &choiceExpr{
				pos: position{line: 349, col: 28, offset: 10941},
				alternatives: []interface{}{
					&ruleRefExpr{
						pos:  position{line: 349, col: 28, offset: 10941},
						name: "DecimalLiteral",
					},
					&ruleRefExpr{
						pos:  position{line: 349, col: 45, offset: 10958},
						name: "OctalLiteral",
					},
					&ruleRefExpr{
						pos:  position{line: 349, col: 60, offset: 10973},
						name: "HexadecimalLiteral",
					},
				},
			},
		},
		{
			name: "DecimalLiteral",
			pos:  position{line: 350, col: 1, offset: 10993},
			expr: &actionExpr{
				pos: position{line: 350, col: 28, offset: 11020},
				run: (*parser).callonDecimalLiteral1,
				expr: &seqExpr{
					pos: position{line: 350, col: 28, offset: 11020},
					exprs: []interface{}{
						&ruleRefExpr{
							pos:  position{line: 350, col: 28, offset: 11020},
							name: "NonzeroDigit",
						},
						&zeroOrMoreExpr{
							pos: position{line: 350, col: 41, offset: 11033},
							expr: &ruleRefExpr{
								pos:  position{line: 350, col: 41, offset: 11033},
								name: "Digit",
							},
						},
					},
				},
			},
		},
		{
			name: "OctalLiteral",
			pos:  position{line: 351, col: 1, offset: 11101},
			expr: &actionExpr{
				pos: position{line: 351, col: 28, offset: 11128},
				run: (*parser).callonOctalLiteral1,
				expr: &seqExpr{
					pos: position{line: 351, col: 28, offset: 11128},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 351, col: 28, offset: 11128},
							val:        "0",
							ignoreCase: false,
						},
						&oneOrMoreExpr{
							pos: position{line: 351, col: 32, offset: 11132},
							expr: &ruleRefExpr{
								pos:  position{line: 351, col: 32, offset: 11132},
								name: "OctalDigit",
							},
						},
					},
				},
			},
		},
		{
			name: "HexadecimalLiteral",
			pos:  position{line: 352, col: 1, offset: 11209},
			expr: &actionExpr{
				pos: position{line: 352, col: 28, offset: 11236},
				run: (*parser).callonHexadecimalLiteral1,
				expr: &seqExpr{
					pos: position{line: 352, col: 28, offset: 11236},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 352, col: 28, offset: 11236},
							val:        "0",
							ignoreCase: false,
						},
						&charClassMatcher{
							pos:        position{line: 352, col: 32, offset: 11240},
							val:        "[xX]",
							chars:      []rune{'x', 'X'},
							ignoreCase: false,
							inverted:   false,
						},
						&oneOrMoreExpr{
							pos: position{line: 352, col: 38, offset: 11246},
							expr: &ruleRefExpr{
								pos:  position{line: 352, col: 38, offset: 11246},
								name: "HexadecimalDigit",
							},
						},
					},
				},
			},
		},
		{
			name: "NonzeroDigit",
			pos:  position{line: 353, col: 1, offset: 11317},
			expr: &charClassMatcher{
				pos:        position{line: 353, col: 17, offset: 11333},
				val:        "[1-9]",
				ranges:     []rune{'1', '9'},
				ignoreCase: false,
				inverted:   false,
			},
		},
		{
			name: "OctalDigit",
			pos:  position{line: 354, col: 1, offset: 11340},
			expr: &charClassMatcher{
				pos:        position{line: 354, col: 15, offset: 11354},
				val:        "[0-7]",
				ranges:     []rune{'0', '7'},
				ignoreCase: false,
				inverted:   false,
			},
		},
		{
			name: "ControlLine",
			pos:  position{line: 357, col: 1, offset: 11366},
			expr: &actionExpr{
				pos: position{line: 357, col: 20, offset: 11385},
				run: (*parser).callonControlLine1,
				expr: &seqExpr{
					pos: position{line: 357, col: 20, offset: 11385},
					exprs: []interface{}{
						&ruleRefExpr{
							pos:  position{line: 357, col: 20, offset: 11385},
							name: "_",
						},
						&litMatcher{
							pos:        position{line: 357, col: 22, offset: 11387},
							val:        "#",
							ignoreCase: false,
						},
						&labeledExpr{
							pos:   position{line: 357, col: 26, offset: 11391},
							label: "controlOp",
							expr: &choiceExpr{
								pos: position{line: 357, col: 37, offset: 11402},
								alternatives: []interface{}{
									&ruleRefExpr{
										pos:  position{line: 357, col: 37, offset: 11402},
										name: "IncludeOp",
									},
									&ruleRefExpr{
										pos:  position{line: 357, col: 49, offset: 11414},
										name: "DefineOp",
									},
									&ruleRefExpr{
										pos:  position{line: 357, col: 60, offset: 11425},
										name: "UndefOp",
									},
									&ruleRefExpr{
										pos:  position{line: 357, col: 70, offset: 11435},
										name: "ErrorOp",
									},
								},
							},
						},
						&ruleRefExpr{
							pos:  position{line: 357, col: 80, offset: 11445},
							name: "_",
						},
						&ruleRefExpr{
							pos:  position{line: 357, col: 82, offset: 11447},
							name: "NL",
						},
					},
				},
			},
		},
		{
			name: "IncludeOp",
			pos:  position{line: 358, col: 1, offset: 11486},
			expr: &actionExpr{
				pos: position{line: 358, col: 20, offset: 11505},
				run: (*parser).callonIncludeOp1,
				expr: &seqExpr{
					pos: position{line: 358, col: 20, offset: 11505},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 358, col: 20, offset: 11505},
							val:        "include",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 358, col: 30, offset: 11515},
							name: "_",
						},
						&labeledExpr{
							pos:   position{line: 358, col: 32, offset: 11517},
							label: "qfilename",
							expr: &ruleRefExpr{
								pos:  position{line: 358, col: 42, offset: 11527},
								name: "PPTokens",
							},
						},
					},
				},
			},
		},
		{
			name: "DefineOp",
			pos:  position{line: 363, col: 1, offset: 11647},
			expr: &actionExpr{
				pos: position{line: 363, col: 20, offset: 11666},
				run: (*parser).callonDefineOp1,
				expr: &seqExpr{
					pos: position{line: 363, col: 20, offset: 11666},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 363, col: 20, offset: 11666},
							val:        "define",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 363, col: 29, offset: 11675},
							name: "_",
						},
						&labeledExpr{
							pos:   position{line: 363, col: 32, offset: 11678},
							label: "name",
							expr: &ruleRefExpr{
								pos:  position{line: 363, col: 37, offset: 11683},
								name: "Identifier",
							},
						},
						&zeroOrOneExpr{
							pos: position{line: 363, col: 48, offset: 11694},
							expr: &seqExpr{
								pos: position{line: 363, col: 49, offset: 11695},
								exprs: []interface{}{
									&litMatcher{
										pos:        position{line: 363, col: 49, offset: 11695},
										val:        "(",
										ignoreCase: false,
									},
									&ruleRefExpr{
										pos:  position{line: 363, col: 53, offset: 11699},
										name: "_",
									},
									&zeroOrOneExpr{
										pos: position{line: 363, col: 56, offset: 11702},
										expr: &ruleRefExpr{
											pos:  position{line: 363, col: 56, offset: 11702},
											name: "IdentifierList",
										},
									},
									&ruleRefExpr{
										pos:  position{line: 363, col: 72, offset: 11718},
										name: "_",
									},
									&litMatcher{
										pos:        position{line: 363, col: 74, offset: 11720},
										val:        ")",
										ignoreCase: false,
									},
								},
							},
						},
						&ruleRefExpr{
							pos:  position{line: 363, col: 80, offset: 11726},
							name: "_",
						},
						&labeledExpr{
							pos:   position{line: 363, col: 82, offset: 11728},
							label: "val",
							expr: &ruleRefExpr{
								pos:  position{line: 363, col: 86, offset: 11732},
								name: "ReplacementList",
							},
						},
					},
				},
			},
		},
		{
			name: "UndefOp",
			pos:  position{line: 367, col: 1, offset: 11833},
			expr: &actionExpr{
				pos: position{line: 367, col: 20, offset: 11852},
				run: (*parser).callonUndefOp1,
				expr: &seqExpr{
					pos: position{line: 367, col: 20, offset: 11852},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 367, col: 20, offset: 11852},
							val:        "undef",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 367, col: 28, offset: 11860},
							name: "_",
						},
						&labeledExpr{
							pos:   position{line: 367, col: 30, offset: 11862},
							label: "name",
							expr: &ruleRefExpr{
								pos:  position{line: 367, col: 35, offset: 11867},
								name: "Identifier",
							},
						},
					},
				},
			},
		},
		{
			name: "ErrorOp",
			pos:  position{line: 371, col: 1, offset: 11943},
			expr: &actionExpr{
				pos: position{line: 371, col: 20, offset: 11962},
				run: (*parser).callonErrorOp1,
				expr: &seqExpr{
					pos: position{line: 371, col: 20, offset: 11962},
					exprs: []interface{}{
						&litMatcher{
							pos:        position{line: 371, col: 20, offset: 11962},
							val:        "error",
							ignoreCase: false,
						},
						&ruleRefExpr{
							pos:  position{line: 371, col: 28, offset: 11970},
							name: "_",
						},
						&zeroOrOneExpr{
							pos: position{line: 371, col: 30, offset: 11972},
							expr: &ruleRefExpr{
								pos:  position{line: 371, col: 30, offset: 11972},
								name: "PPTokens",
							},
						},
					},
				},
			},
		},
		{
			name: "IdentifierList",
			pos:  position{line: 375, col: 1, offset: 12013},
			expr: &seqExpr{
				pos: position{line: 375, col: 19, offset: 12031},
				exprs: []interface{}{
					&ruleRefExpr{
						pos:  position{line: 375, col: 19, offset: 12031},
						name: "Identifier",
					},
					&ruleRefExpr{
						pos:  position{line: 375, col: 30, offset: 12042},
						name: "_",
					},
					&zeroOrMoreExpr{
						pos: position{line: 375, col: 32, offset: 12044},
						expr: &seqExpr{
							pos: position{line: 375, col: 33, offset: 12045},
							exprs: []interface{}{
								&litMatcher{
									pos:        position{line: 375, col: 33, offset: 12045},
									val:        ",",
									ignoreCase: false,
								},
								&ruleRefExpr{
									pos:  position{line: 375, col: 37, offset: 12049},
									name: "_",
								},
								&ruleRefExpr{
									pos:  position{line: 375, col: 40, offset: 12052},
									name: "Identifier",
								},
							},
						},
					},
				},
			},
		},
		{
			name: "ReplacementList",
			pos:  position{line: 376, col: 1, offset: 12068},
			expr: &actionExpr{
				pos: position{line: 376, col: 20, offset: 12087},
				run: (*parser).callonReplacementList1,
				expr: &labeledExpr{
					pos:   position{line: 376, col: 20, offset: 12087},
					label: "val",
					expr: &zeroOrOneExpr{
						pos: position{line: 376, col: 24, offset: 12091},
						expr: &ruleRefExpr{
							pos:  position{line: 376, col: 24, offset: 12091},
							name: "PPTokens",
						},
					},
				},
			},
		},
		{
			name: "EOF",
			pos:  position{line: 378, col: 1, offset: 12153},
			expr: &actionExpr{
				pos: position{line: 378, col: 8, offset: 12160},
				run: (*parser).callonEOF1,
				expr: &notExpr{
					pos: position{line: 378, col: 8, offset: 12160},
					expr: &anyMatcher{
						line: 378, col: 9, offset: 12161,
					},
				},
			},
		},
		{
			name: "NL",
			pos:  position{line: 379, col: 1, offset: 12229},
			expr: &actionExpr{
				pos: position{line: 379, col: 7, offset: 12235},
				run: (*parser).callonNL1,
				expr: &seqExpr{
					pos: position{line: 379, col: 7, offset: 12235},
					exprs: []interface{}{
						&zeroOrOneExpr{
							pos: position{line: 379, col: 7, offset: 12235},
							expr: &litMatcher{
								pos:        position{line: 379, col: 7, offset: 12235},
								val:        "\r",
								ignoreCase: false,
							},
						},
						&litMatcher{
							pos:        position{line: 379, col: 13, offset: 12241},
							val:        "\n",
							ignoreCase: false,
						},
					},
				},
			},
		},
		{
			name:        "_",
			displayName: "\"whitespace\"",
			pos:         position{line: 380, col: 1, offset: 12347},
			expr: &actionExpr{
				pos: position{line: 380, col: 19, offset: 12365},
				run: (*parser).callon_1,
				expr: &zeroOrMoreExpr{
					pos: position{line: 380, col: 19, offset: 12365},
					expr: &charClassMatcher{
						pos:        position{line: 380, col: 19, offset: 12365},
						val:        "[ \\t]",
						chars:      []rune{' ', '\t'},
						ignoreCase: false,
						inverted:   false,
					},
				},
			},
		},
	},
}

func (c *current) onPreprocessingFile1(val interface{}) (interface{}, error) {
	return val.(string), nil
}

func (p *parser) callonPreprocessingFile1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPreprocessingFile1(stack["val"])
}

func (c *current) onGroup1(val interface{}) (interface{}, error) {
	return IfaceSliceToString(toIfaceSlice(val)), nil
}

func (p *parser) callonGroup1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onGroup1(stack["val"])
}

func (c *current) onGroupPart1(val interface{}) (interface{}, error) {
	return val.(string), nil
}

func (p *parser) callonGroupPart1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onGroupPart1(stack["val"])
}

func (c *current) onComment1() (interface{}, error) {
	return "", nil
}

func (p *parser) callonComment1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onComment1()
}

func (c *current) onOtherTokens1(val interface{}) (interface{}, error) {
	return IfaceSliceToString(toIfaceSlice(val)), nil
}

func (p *parser) callonOtherTokens1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onOtherTokens1(stack["val"])
}

func (c *current) onPPTokens1(val interface{}) (interface{}, error) {
	return IfaceSliceToString(toIfaceSlice(val)), nil
}

func (p *parser) callonPPTokens1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPPTokens1(stack["val"])
}

func (c *current) onPreprocessingToken1(val interface{}) (interface{}, error) {
	return val.(string), nil
}

func (p *parser) callonPreprocessingToken1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPreprocessingToken1(stack["val"])
}

func (c *current) onANY_NOT_YET_COVERED_NON_WSCHARACTER1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonANY_NOT_YET_COVERED_NON_WSCHARACTER1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onANY_NOT_YET_COVERED_NON_WSCHARACTER1()
}

func (c *current) onIdentifier1() (interface{}, error) {

	v, ok := macrosMap[string(c.text)]
	if ok {
		return v.value, nil
	}
	return string(c.text), nil
}

func (p *parser) callonIdentifier1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onIdentifier1()
}

func (c *current) onCharacterLiteral1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonCharacterLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onCharacterLiteral1()
}

func (c *current) onStringLiteral1(val interface{}) (interface{}, error) {
	return "\"" + val.(string) + "\"", nil
}

func (p *parser) callonStringLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onStringLiteral1(stack["val"])
}

func (c *current) onSCharSequence1(val interface{}) (interface{}, error) {
	return IfaceSliceToString(toIfaceSlice(val)), nil
}

func (p *parser) callonSCharSequence1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onSCharSequence1(stack["val"])
}

func (c *current) onSChar1(val interface{}) (interface{}, error) {
	return val.(string), nil
}

func (p *parser) callonSChar1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onSChar1(stack["val"])
}

func (c *current) onAnyExceptNlDqBs1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonAnyExceptNlDqBs1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onAnyExceptNlDqBs1()
}

func (c *current) onEscapeSequence1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonEscapeSequence1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEscapeSequence1()
}

func (c *current) onEmbeddedLiteral1(val interface{}) (interface{}, error) {

	if val != nil {
		return "[" + val.(string) + "]", nil
	}
	return string(c.text), nil
}

func (p *parser) callonEmbeddedLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEmbeddedLiteral1(stack["val"])
}

func (c *current) onEmbeddedExpr1(val interface{}) (interface{}, error) {

	ex := toIfaceSlice(val)
	ex1 := toIfaceSlice(ex)

	s := ""
	for i := range ex1 {
		s = s + toIfaceSlice(ex1[i])[1].(string)
	}
	return s, nil
}

func (p *parser) callonEmbeddedExpr1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEmbeddedExpr1(stack["val"])
}

func (c *current) onIfSection1(ifval, elifval, elseval interface{}) (interface{}, error) {

	if ifval.(string) != "" {
		return ifval.(string), nil
	} else {
		v := toIfaceSlice(elifval)
		for i := range v {
			if v[i].(string) != "" {
				return v[i].(string), nil
			}
		}
		if elseval != nil {
			return elseval.(string), nil
		}
	}
	return "", nil
}

func (p *parser) callonIfSection1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onIfSection1(stack["ifval"], stack["elifval"], stack["elseval"])
}

func (c *current) onIfGroup1(ex, group interface{}) (interface{}, error) {

	exs := toIfaceSlice(ex)
	exop := Uint8SliceToString(exs[0])
	if exop == "ifdef" {
		_, ok := macrosMap[exs[2].(string)]
		if ok {
			return group.(string), nil
		}
		os.Stderr.WriteString("Macro " + exs[2].(string) + " is not defined")
		return "", nil
	} else if exop == "if" {
		if exs[2].(int) != 0 {
			return group.(string), nil
		}
	}

	return "", nil
}

func (p *parser) callonIfGroup1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onIfGroup1(stack["ex"], stack["group"])
}

func (c *current) onIfDefMacro1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonIfDefMacro1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onIfDefMacro1()
}

func (c *current) onElIfGroup1(val, group interface{}) (interface{}, error) {

	if val.(int) != 0 {
		return group.(string), nil
	}
	return "", nil
}

func (p *parser) callonElIfGroup1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onElIfGroup1(stack["val"], stack["group"])
}

func (c *current) onElseGroup1(group interface{}) (interface{}, error) {
	return group.(string), nil
}

func (p *parser) callonElseGroup1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onElseGroup1(stack["group"])
}

func (c *current) onEndifLine1() (interface{}, error) {
	return "", nil
}

func (p *parser) callonEndifLine1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEndifLine1()
}

func (c *current) onPPExpression1(ex interface{}) (interface{}, error) {
	return ex.(int), nil
}

func (p *parser) callonPPExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPPExpression1(stack["ex"])
}

func (c *current) onConditionalExpression1(ex interface{}) (interface{}, error) {
	return ex.(int), nil
}

func (p *parser) callonConditionalExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onConditionalExpression1(stack["ex"])
}

func (c *current) onLogicalOrExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonLogicalOrExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onLogicalOrExpression1(stack["ex"], stack["rest"])
}

func (c *current) onLogicalAndExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonLogicalAndExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onLogicalAndExpression1(stack["ex"], stack["rest"])
}

func (c *current) onInclusiveOrExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonInclusiveOrExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onInclusiveOrExpression1(stack["ex"], stack["rest"])
}

func (c *current) onExclusiveOrExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonExclusiveOrExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onExclusiveOrExpression1(stack["ex"], stack["rest"])
}

func (c *current) onAndExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonAndExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onAndExpression1(stack["ex"], stack["rest"])
}

func (c *current) onEqualityExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonEqualityExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEqualityExpression1(stack["ex"], stack["rest"])
}

func (c *current) onRelationalExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonRelationalExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onRelationalExpression1(stack["ex"], stack["rest"])
}

func (c *current) onShiftExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonShiftExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onShiftExpression1(stack["ex"], stack["rest"])
}

func (c *current) onAdditiveExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonAdditiveExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onAdditiveExpression1(stack["ex"], stack["rest"])
}

func (c *current) onMultiplicativeExpression1(ex, rest interface{}) (interface{}, error) {
	return eval(ex, rest), nil
}

func (p *parser) callonMultiplicativeExpression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onMultiplicativeExpression1(stack["ex"], stack["rest"])
}

func (c *current) onUnary_Expression1(unop, val interface{}) (interface{}, error) {

	pe := val.(int)
	if unop == nil {
		return pe, nil
	}

	switch unop.(string) {
	case "+":
		pe = pe
	case "-":
		pe = -pe
	case "!":
		pe = BoolToInt(pe == 0)
	case "~":
		pe = ^pe
	}
	return pe, nil

}

func (p *parser) callonUnary_Expression1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onUnary_Expression1(stack["unop"], stack["val"])
}

func (c *current) onUnaryOperator1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonUnaryOperator1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onUnaryOperator1()
}

func (c *current) onPrimaryExpression2(val interface{}) (interface{}, error) {

	_, ok := macrosMap[val.(string)]
	if ok {
		return 1, nil
	}
	return 0, nil
}

func (p *parser) callonPrimaryExpression2() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPrimaryExpression2(stack["val"])
}

func (c *current) onPrimaryExpression11() (interface{}, error) {
	return strconv.ParseUint(string(c.text), 0, 64)
}

func (p *parser) callonPrimaryExpression11() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPrimaryExpression11()
}

func (c *current) onPrimaryExpression13(val interface{}) (interface{}, error) {

	return val.(int), nil
}

func (p *parser) callonPrimaryExpression13() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPrimaryExpression13(stack["val"])
}

func (c *current) onPrimaryExpression21(val interface{}) (interface{}, error) {

	fmt.Println(val.(string))
	v, ok := macrosMap[val.(string)]
	if ok {
		return v, nil
	}
	return 0, fmt.Errorf("Macro %q is not defined", val.(string))
}

func (p *parser) callonPrimaryExpression21() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onPrimaryExpression21(stack["val"])
}

func (c *current) onDecimalLiteral1() (interface{}, error) {
	return strconv.ParseUint(string(c.text), 0, 64)
}

func (p *parser) callonDecimalLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onDecimalLiteral1()
}

func (c *current) onOctalLiteral1() (interface{}, error) {
	return strconv.ParseUint(string(c.text), 0, 64)
}

func (p *parser) callonOctalLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onOctalLiteral1()
}

func (c *current) onHexadecimalLiteral1() (interface{}, error) {
	return strconv.ParseUint(string(c.text), 0, 64)
}

func (p *parser) callonHexadecimalLiteral1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onHexadecimalLiteral1()
}

func (c *current) onControlLine1(controlOp interface{}) (interface{}, error) {
	return controlOp.(string), nil
}

func (p *parser) callonControlLine1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onControlLine1(stack["controlOp"])
}

func (c *current) onIncludeOp1(qfilename interface{}) (interface{}, error) {

	sl := qfilename.(string)
	filename, _ := strconv.Unquote(sl)
	return parseInclude(filename)
}

func (p *parser) callonIncludeOp1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onIncludeOp1(stack["qfilename"])
}

func (c *current) onDefineOp1(name, val interface{}) (interface{}, error) {

	macrosMap.add(ppMacro{name.(string), val.(string)})
	return "\n", nil
}

func (p *parser) callonDefineOp1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onDefineOp1(stack["name"], stack["val"])
}

func (c *current) onUndefOp1(name interface{}) (interface{}, error) {

	macrosMap.delete(name.(string))
	return "\n", nil
}

func (p *parser) callonUndefOp1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onUndefOp1(stack["name"])
}

func (c *current) onErrorOp1() (interface{}, error) {

	return "\n", nil
}

func (p *parser) callonErrorOp1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onErrorOp1()
}

func (c *current) onReplacementList1(val interface{}) (interface{}, error) {
	return val.(string), nil
}

func (p *parser) callonReplacementList1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onReplacementList1(stack["val"])
}

func (c *current) onEOF1() (interface{}, error) {
	return "", nil
}

func (p *parser) callonEOF1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onEOF1()
}

func (c *current) onNL1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callonNL1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.onNL1()
}

func (c *current) on_1() (interface{}, error) {
	return string(c.text), nil
}

func (p *parser) callon_1() (interface{}, error) {
	stack := p.vstack[len(p.vstack)-1]
	_ = stack
	return p.cur.on_1()
}

var (
	// errNoRule is returned when the grammar to parse has no rule.
	errNoRule = errors.New("grammar has no rule")

	// errInvalidEncoding is returned when the source is not properly
	// utf8-encoded.
	errInvalidEncoding = errors.New("invalid encoding")

	// errNoMatch is returned if no match could be found.
	errNoMatch = errors.New("no match found")
)

// Option is a function that can set an option on the parser. It returns
// the previous setting as an Option.
type Option func(*parser) Option

// Debug creates an Option to set the debug flag to b. When set to true,
// debugging information is printed to stdout while parsing.
//
// The default is false.
func Debug(b bool) Option {
	return func(p *parser) Option {
		old := p.debug
		p.debug = b
		return Debug(old)
	}
}

// Memoize creates an Option to set the memoize flag to b. When set to true,
// the parser will cache all results so each expression is evaluated only
// once. This guarantees linear parsing time even for pathological cases,
// at the expense of more memory and slower times for typical cases.
//
// The default is false.
func Memoize(b bool) Option {
	return func(p *parser) Option {
		old := p.memoize
		p.memoize = b
		return Memoize(old)
	}
}

// Recover creates an Option to set the recover flag to b. When set to
// true, this causes the parser to recover from panics and convert it
// to an error. Setting it to false can be useful while debugging to
// access the full stack trace.
//
// The default is true.
func Recover(b bool) Option {
	return func(p *parser) Option {
		old := p.recover
		p.recover = b
		return Recover(old)
	}
}

// ParseFile parses the file identified by filename.
func ParseFile(filename string, opts ...Option) (interface{}, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ParseReader(filename, f, opts...)
}

// ParseReader parses the data from r using filename as information in the
// error messages.
func ParseReader(filename string, r io.Reader, opts ...Option) (interface{}, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return Parse(filename, b, opts...)
}

// Parse parses the data from b using filename as information in the
// error messages.
func Parse(filename string, b []byte, opts ...Option) (interface{}, error) {
	return newParser(filename, b, opts...).parse(g)
}

// position records a position in the text.
type position struct {
	line, col, offset int
}

func (p position) String() string {
	return fmt.Sprintf("%d:%d [%d]", p.line, p.col, p.offset)
}

// savepoint stores all state required to go back to this point in the
// parser.
type savepoint struct {
	position
	rn rune
	w  int
}

type current struct {
	pos  position // start position of the match
	text []byte   // raw text of the match
}

// the AST types...

type grammar struct {
	pos   position
	rules []*rule
}

type rule struct {
	pos         position
	name        string
	displayName string
	expr        interface{}
}

type choiceExpr struct {
	pos          position
	alternatives []interface{}
}

type actionExpr struct {
	pos  position
	expr interface{}
	run  func(*parser) (interface{}, error)
}

type seqExpr struct {
	pos   position
	exprs []interface{}
}

type labeledExpr struct {
	pos   position
	label string
	expr  interface{}
}

type expr struct {
	pos  position
	expr interface{}
}

type andExpr expr
type notExpr expr
type zeroOrOneExpr expr
type zeroOrMoreExpr expr
type oneOrMoreExpr expr

type ruleRefExpr struct {
	pos  position
	name string
}

type andCodeExpr struct {
	pos position
	run func(*parser) (bool, error)
}

type notCodeExpr struct {
	pos position
	run func(*parser) (bool, error)
}

type litMatcher struct {
	pos        position
	val        string
	ignoreCase bool
}

type charClassMatcher struct {
	pos        position
	val        string
	chars      []rune
	ranges     []rune
	classes    []*unicode.RangeTable
	ignoreCase bool
	inverted   bool
}

type anyMatcher position

// errList cumulates the errors found by the parser.
type errList []error

func (e *errList) add(err error) {
	*e = append(*e, err)
}

func (e errList) err() error {
	if len(e) == 0 {
		return nil
	}
	e.dedupe()
	return e
}

func (e *errList) dedupe() {
	var cleaned []error
	set := make(map[string]bool)
	for _, err := range *e {
		if msg := err.Error(); !set[msg] {
			set[msg] = true
			cleaned = append(cleaned, err)
		}
	}
	*e = cleaned
}

func (e errList) Error() string {
	switch len(e) {
	case 0:
		return ""
	case 1:
		return e[0].Error()
	default:
		var buf bytes.Buffer

		for i, err := range e {
			if i > 0 {
				buf.WriteRune('\n')
			}
			buf.WriteString(err.Error())
		}
		return buf.String()
	}
}

// parserError wraps an error with a prefix indicating the rule in which
// the error occurred. The original error is stored in the Inner field.
type parserError struct {
	Inner  error
	pos    position
	prefix string
}

// Error returns the error message.
func (p *parserError) Error() string {
	return p.prefix + ": " + p.Inner.Error()
}

// newParser creates a parser with the specified input source and options.
func newParser(filename string, b []byte, opts ...Option) *parser {
	p := &parser{
		filename: filename,
		errs:     new(errList),
		data:     b,
		pt:       savepoint{position: position{line: 1}},
		recover:  true,
	}
	p.setOptions(opts)
	return p
}

// setOptions applies the options to the parser.
func (p *parser) setOptions(opts []Option) {
	for _, opt := range opts {
		opt(p)
	}
}

type resultTuple struct {
	v   interface{}
	b   bool
	end savepoint
}

type parser struct {
	filename string
	pt       savepoint
	cur      current

	data []byte
	errs *errList

	recover bool
	debug   bool
	depth   int

	memoize bool
	// memoization table for the packrat algorithm:
	// map[offset in source] map[expression or rule] {value, match}
	memo map[int]map[interface{}]resultTuple

	// rules table, maps the rule identifier to the rule node
	rules map[string]*rule
	// variables stack, map of label to value
	vstack []map[string]interface{}
	// rule stack, allows identification of the current rule in errors
	rstack []*rule

	// stats
	exprCnt int
}

// push a variable set on the vstack.
func (p *parser) pushV() {
	if cap(p.vstack) == len(p.vstack) {
		// create new empty slot in the stack
		p.vstack = append(p.vstack, nil)
	} else {
		// slice to 1 more
		p.vstack = p.vstack[:len(p.vstack)+1]
	}

	// get the last args set
	m := p.vstack[len(p.vstack)-1]
	if m != nil && len(m) == 0 {
		// empty map, all good
		return
	}

	m = make(map[string]interface{})
	p.vstack[len(p.vstack)-1] = m
}

// pop a variable set from the vstack.
func (p *parser) popV() {
	// if the map is not empty, clear it
	m := p.vstack[len(p.vstack)-1]
	if len(m) > 0 {
		// GC that map
		p.vstack[len(p.vstack)-1] = nil
	}
	p.vstack = p.vstack[:len(p.vstack)-1]
}

func (p *parser) print(prefix, s string) string {
	if !p.debug {
		return s
	}

	fmt.Printf("%s %d:%d:%d: %s [%#U]\n",
		prefix, p.pt.line, p.pt.col, p.pt.offset, s, p.pt.rn)
	return s
}

func (p *parser) in(s string) string {
	p.depth++
	return p.print(strings.Repeat(" ", p.depth)+">", s)
}

func (p *parser) out(s string) string {
	p.depth--
	return p.print(strings.Repeat(" ", p.depth)+"<", s)
}

func (p *parser) addErr(err error) {
	p.addErrAt(err, p.pt.position)
}

func (p *parser) addErrAt(err error, pos position) {
	var buf bytes.Buffer
	if p.filename != "" {
		buf.WriteString(p.filename)
	}
	if buf.Len() > 0 {
		buf.WriteString(":")
	}
	buf.WriteString(fmt.Sprintf("%d:%d (%d)", pos.line, pos.col, pos.offset))
	if len(p.rstack) > 0 {
		if buf.Len() > 0 {
			buf.WriteString(": ")
		}
		rule := p.rstack[len(p.rstack)-1]
		if rule.displayName != "" {
			buf.WriteString("rule " + rule.displayName)
		} else {
			buf.WriteString("rule " + rule.name)
		}
	}
	pe := &parserError{Inner: err, pos: pos, prefix: buf.String()}
	p.errs.add(pe)
}

// read advances the parser to the next rune.
func (p *parser) read() {
	p.pt.offset += p.pt.w
	rn, n := utf8.DecodeRune(p.data[p.pt.offset:])
	p.pt.rn = rn
	p.pt.w = n
	p.pt.col++
	if rn == '\n' {
		p.pt.line++
		p.pt.col = 0
	}

	if rn == utf8.RuneError {
		if n > 0 {
			p.addErr(errInvalidEncoding)
		}
	}
}

// restore parser position to the savepoint pt.
func (p *parser) restore(pt savepoint) {
	if p.debug {
		defer p.out(p.in("restore"))
	}
	if pt.offset == p.pt.offset {
		return
	}
	p.pt = pt
}

// get the slice of bytes from the savepoint start to the current position.
func (p *parser) sliceFrom(start savepoint) []byte {
	return p.data[start.position.offset:p.pt.position.offset]
}

func (p *parser) getMemoized(node interface{}) (resultTuple, bool) {
	if len(p.memo) == 0 {
		return resultTuple{}, false
	}
	m := p.memo[p.pt.offset]
	if len(m) == 0 {
		return resultTuple{}, false
	}
	res, ok := m[node]
	return res, ok
}

func (p *parser) setMemoized(pt savepoint, node interface{}, tuple resultTuple) {
	if p.memo == nil {
		p.memo = make(map[int]map[interface{}]resultTuple)
	}
	m := p.memo[pt.offset]
	if m == nil {
		m = make(map[interface{}]resultTuple)
		p.memo[pt.offset] = m
	}
	m[node] = tuple
}

func (p *parser) buildRulesTable(g *grammar) {
	p.rules = make(map[string]*rule, len(g.rules))
	for _, r := range g.rules {
		p.rules[r.name] = r
	}
}

func (p *parser) parse(g *grammar) (val interface{}, err error) {
	if len(g.rules) == 0 {
		p.addErr(errNoRule)
		return nil, p.errs.err()
	}

	// TODO : not super critical but this could be generated
	p.buildRulesTable(g)

	if p.recover {
		// panic can be used in action code to stop parsing immediately
		// and return the panic as an error.
		defer func() {
			if e := recover(); e != nil {
				if p.debug {
					defer p.out(p.in("panic handler"))
				}
				val = nil
				switch e := e.(type) {
				case error:
					p.addErr(e)
				default:
					p.addErr(fmt.Errorf("%v", e))
				}
				err = p.errs.err()
			}
		}()
	}

	// start rule is rule [0]
	p.read() // advance to first rune
	val, ok := p.parseRule(g.rules[0])
	if !ok {
		if len(*p.errs) == 0 {
			// make sure this doesn't go out silently
			p.addErr(errNoMatch)
		}
		return nil, p.errs.err()
	}
	return val, p.errs.err()
}

func (p *parser) parseRule(rule *rule) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseRule " + rule.name))
	}

	if p.memoize {
		res, ok := p.getMemoized(rule)
		if ok {
			p.restore(res.end)
			return res.v, res.b
		}
	}

	start := p.pt
	p.rstack = append(p.rstack, rule)
	p.pushV()
	val, ok := p.parseExpr(rule.expr)
	p.popV()
	p.rstack = p.rstack[:len(p.rstack)-1]
	if ok && p.debug {
		p.print(strings.Repeat(" ", p.depth)+"MATCH", string(p.sliceFrom(start)))
	}

	if p.memoize {
		p.setMemoized(start, rule, resultTuple{val, ok, p.pt})
	}
	return val, ok
}

func (p *parser) parseExpr(expr interface{}) (interface{}, bool) {
	var pt savepoint
	var ok bool

	if p.memoize {
		res, ok := p.getMemoized(expr)
		if ok {
			p.restore(res.end)
			return res.v, res.b
		}
		pt = p.pt
	}

	p.exprCnt++
	var val interface{}
	switch expr := expr.(type) {
	case *actionExpr:
		val, ok = p.parseActionExpr(expr)
	case *andCodeExpr:
		val, ok = p.parseAndCodeExpr(expr)
	case *andExpr:
		val, ok = p.parseAndExpr(expr)
	case *anyMatcher:
		val, ok = p.parseAnyMatcher(expr)
	case *charClassMatcher:
		val, ok = p.parseCharClassMatcher(expr)
	case *choiceExpr:
		val, ok = p.parseChoiceExpr(expr)
	case *labeledExpr:
		val, ok = p.parseLabeledExpr(expr)
	case *litMatcher:
		val, ok = p.parseLitMatcher(expr)
	case *notCodeExpr:
		val, ok = p.parseNotCodeExpr(expr)
	case *notExpr:
		val, ok = p.parseNotExpr(expr)
	case *oneOrMoreExpr:
		val, ok = p.parseOneOrMoreExpr(expr)
	case *ruleRefExpr:
		val, ok = p.parseRuleRefExpr(expr)
	case *seqExpr:
		val, ok = p.parseSeqExpr(expr)
	case *zeroOrMoreExpr:
		val, ok = p.parseZeroOrMoreExpr(expr)
	case *zeroOrOneExpr:
		val, ok = p.parseZeroOrOneExpr(expr)
	default:
		panic(fmt.Sprintf("unknown expression type %T", expr))
	}
	if p.memoize {
		p.setMemoized(pt, expr, resultTuple{val, ok, p.pt})
	}
	return val, ok
}

func (p *parser) parseActionExpr(act *actionExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseActionExpr"))
	}

	start := p.pt
	val, ok := p.parseExpr(act.expr)
	if ok {
		p.cur.pos = start.position
		p.cur.text = p.sliceFrom(start)
		actVal, err := act.run(p)
		if err != nil {
			p.addErrAt(err, start.position)
		}
		val = actVal
	}
	if ok && p.debug {
		p.print(strings.Repeat(" ", p.depth)+"MATCH", string(p.sliceFrom(start)))
	}
	return val, ok
}

func (p *parser) parseAndCodeExpr(and *andCodeExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseAndCodeExpr"))
	}

	ok, err := and.run(p)
	if err != nil {
		p.addErr(err)
	}
	return nil, ok
}

func (p *parser) parseAndExpr(and *andExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseAndExpr"))
	}

	pt := p.pt
	p.pushV()
	_, ok := p.parseExpr(and.expr)
	p.popV()
	p.restore(pt)
	return nil, ok
}

func (p *parser) parseAnyMatcher(any *anyMatcher) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseAnyMatcher"))
	}

	if p.pt.rn != utf8.RuneError {
		start := p.pt
		p.read()
		return p.sliceFrom(start), true
	}
	return nil, false
}

func (p *parser) parseCharClassMatcher(chr *charClassMatcher) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseCharClassMatcher"))
	}

	cur := p.pt.rn
	// can't match EOF
	if cur == utf8.RuneError {
		return nil, false
	}
	start := p.pt
	if chr.ignoreCase {
		cur = unicode.ToLower(cur)
	}

	// try to match in the list of available chars
	for _, rn := range chr.chars {
		if rn == cur {
			if chr.inverted {
				return nil, false
			}
			p.read()
			return p.sliceFrom(start), true
		}
	}

	// try to match in the list of ranges
	for i := 0; i < len(chr.ranges); i += 2 {
		if cur >= chr.ranges[i] && cur <= chr.ranges[i+1] {
			if chr.inverted {
				return nil, false
			}
			p.read()
			return p.sliceFrom(start), true
		}
	}

	// try to match in the list of Unicode classes
	for _, cl := range chr.classes {
		if unicode.Is(cl, cur) {
			if chr.inverted {
				return nil, false
			}
			p.read()
			return p.sliceFrom(start), true
		}
	}

	if chr.inverted {
		p.read()
		return p.sliceFrom(start), true
	}
	return nil, false
}

func (p *parser) parseChoiceExpr(ch *choiceExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseChoiceExpr"))
	}

	for _, alt := range ch.alternatives {
		p.pushV()
		val, ok := p.parseExpr(alt)
		p.popV()
		if ok {
			return val, ok
		}
	}
	return nil, false
}

func (p *parser) parseLabeledExpr(lab *labeledExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseLabeledExpr"))
	}

	p.pushV()
	val, ok := p.parseExpr(lab.expr)
	p.popV()
	if ok && lab.label != "" {
		m := p.vstack[len(p.vstack)-1]
		m[lab.label] = val
	}
	return val, ok
}

func (p *parser) parseLitMatcher(lit *litMatcher) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseLitMatcher"))
	}

	start := p.pt
	for _, want := range lit.val {
		cur := p.pt.rn
		if lit.ignoreCase {
			cur = unicode.ToLower(cur)
		}
		if cur != want {
			p.restore(start)
			return nil, false
		}
		p.read()
	}
	return p.sliceFrom(start), true
}

func (p *parser) parseNotCodeExpr(not *notCodeExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseNotCodeExpr"))
	}

	ok, err := not.run(p)
	if err != nil {
		p.addErr(err)
	}
	return nil, !ok
}

func (p *parser) parseNotExpr(not *notExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseNotExpr"))
	}

	pt := p.pt
	p.pushV()
	_, ok := p.parseExpr(not.expr)
	p.popV()
	p.restore(pt)
	return nil, !ok
}

func (p *parser) parseOneOrMoreExpr(expr *oneOrMoreExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseOneOrMoreExpr"))
	}

	var vals []interface{}

	for {
		p.pushV()
		val, ok := p.parseExpr(expr.expr)
		p.popV()
		if !ok {
			if len(vals) == 0 {
				// did not match once, no match
				return nil, false
			}
			return vals, true
		}
		vals = append(vals, val)
	}
}

func (p *parser) parseRuleRefExpr(ref *ruleRefExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseRuleRefExpr " + ref.name))
	}

	if ref.name == "" {
		panic(fmt.Sprintf("%s: invalid rule: missing name", ref.pos))
	}

	rule := p.rules[ref.name]
	if rule == nil {
		p.addErr(fmt.Errorf("undefined rule: %s", ref.name))
		return nil, false
	}
	return p.parseRule(rule)
}

func (p *parser) parseSeqExpr(seq *seqExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseSeqExpr"))
	}

	var vals []interface{}

	pt := p.pt
	for _, expr := range seq.exprs {
		val, ok := p.parseExpr(expr)
		if !ok {
			p.restore(pt)
			return nil, false
		}
		vals = append(vals, val)
	}
	return vals, true
}

func (p *parser) parseZeroOrMoreExpr(expr *zeroOrMoreExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseZeroOrMoreExpr"))
	}

	var vals []interface{}

	for {
		p.pushV()
		val, ok := p.parseExpr(expr.expr)
		p.popV()
		if !ok {
			return vals, true
		}
		vals = append(vals, val)
	}
}

func (p *parser) parseZeroOrOneExpr(expr *zeroOrOneExpr) (interface{}, bool) {
	if p.debug {
		defer p.out(p.in("parseZeroOrOneExpr"))
	}

	p.pushV()
	val, _ := p.parseExpr(expr.expr)
	p.popV()
	// whether it matched or not, consider it a match
	return val, true
}

func rangeTable(class string) *unicode.RangeTable {
	if rt, ok := unicode.Categories[class]; ok {
		return rt
	}
	if rt, ok := unicode.Properties[class]; ok {
		return rt
	}
	if rt, ok := unicode.Scripts[class]; ok {
		return rt
	}

	// cannot happen
	panic(fmt.Sprintf("invalid Unicode class: %s", class))
}
